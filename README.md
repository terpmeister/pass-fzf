# pass-fzf

This extensions uses `fzf` to either copy or show your `{pass-name}` where `{pass-name}` is your selection  
from the `fzf` query.

If you don't have `fzf` installed, check out its github repos for more info at https://github.com/junegunn/fzf  

You will need to have this extension setup via the instructions on https://www.passwordstore.org/ and have  
copied the extension `fzf.bash` in that folder.

If you don't have `PASSWORD_STORE_DIR` defined, it will use  `$HOME/.password-store`

The `-s/--show` mode runs a `pass show {pass-name}`  
The `-c/--copy` mode runs a `pass -c {pass-name}`  (default option)  
The `-p/--preview` mode runs a `pass show`
  
`{pass-name}` is your selection from the `fzf` query.

Using the `-f/--filter` option runs `fzf` with the `--query` and `--exact` options with your value(s)

```
    Usage:
    pass fzf <mode> <option>

    See the README.md file at https://gitlab.com/terpmeister/pass-fzf for more info/updates

    Mode:
        -s, --show              Show the {pass-name}
        -c, --copy              Copy to the clipboard {pass-name} (default Mode)
        -p, --preview           Use fzf `--preview` feature, selecting entry will copy to clipboard
        -h, --help              Show the help information

    Options:
        -f,--filter <value(s)>  Filter selections to <value(s)>. Multiple values in quotes.
```

When you run `pass fzf` it will run a `find` on your `.passowrd-store` folder and format it with `sed`.  
You can then select your `{pass-name}` using `fzf`.

## Prerequisites
* Tested on MacOS, but should also work on most favors of Linux/Unix
* `pass` - https://www.passwordstore.org/
* `fzf` - https://github.com/junegunn/fzf


## Limitations
I have noticed on my Mac that `{pass-name}` with spaces or a ' in it will give me errors. To fix this I replace those with dashes `-` or `_` in my `{pass-name}`.

## Examples

### Copy to clipboard (show all `{pass-name}`)
```
$ pass fzf
/test
----------
Copied /test to clipboard. Will clear in 45 seconds.
```

### Show (show all `{pass-name}`)
```
>$ pass fzf -s
/test
----------
test
website: https://test.xyz
login: test
Some more super secret info
```

### Preview (show all `{pass-name}`)
```
>$ pass fzf -p
```

### Copy to clipboard with filter
```
$ pass fzf -f "test foo"
/test/foo
----------
Copied /test/foo to clipboard. Will clear in 45 seconds.
```

### Show with filter
```
>$ pass fzf -s -f "test foo"
/test/foo
----------
test-foo
website: https://test-foo.xyz
login: test-foo
Some more super secret info
```

### Preview with filter
```
>$ pass fzf -p -f "test foo"
```

## Copyright
```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```