#!/usr/bin/env bash
# pass fzf - Password Store Extension (https://www.passwordstore.org/)
# Copyright (C) 2020 Shawn Turpin terpmeister@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# See the README.md for more install/usage information.


PASSWORD_STORE_DIR="${PASSWORD_STORE_DIR:-$HOME/.password-store}"

cmd_fzf_usage() {
    cat <<-_EOF
    Usage:
    $PROGRAM fzf <mode> <option>

    See the README.md file at https://gitlab.com/terpmeister/pass-fzf for more info/updates

    Mode:
        -s, --show              Show the {pass-name}
        -c, --copy              Copy to the clipboard {pass-name} (default Mode)
        -p, --preview           Use fzf `--preview` feature, selecting entry will copy to clipboard
        -h, --help              Show the help information

    Options:
        -f,--filter <value(s)>  Filter selections to <value(s)>. Multiple values in quotes.

_EOF
    exit 0
}

cmd_fzf() {

    # Parse arguments
    local OPTS SHOW=0 PREVIEW=0
    local FZFQUERY=""
    OPTS="$($GETOPT -o scpf: -l show,copy,preview,filter: -n "$PROGRAM $COMMAND" -- "$@")"
    local ERR=$?
    eval set -- "$OPTS"

    while true; do 
        case "$1" in
            -s|--show ) SHOW=1; shift ;;
            -c|--copy ) SHOW=0; shift ;;
            -p|--preview) PREVIEW=1; shift ;;
            -f|--filter ) FZFQUERY="$2"; shift 2 ;;
            --) shift ; break ;;
        esac
    done

    [[ $ERR -ne 0 ]] && die "$(cmd_fzf_usage)"

    if [[ $PREVIEW = 1 ]]
    then
        FZFFIND=$(find $PASSWORD_STORE_DIR -name "*.gpg" | sed "s!$PASSWORD_STORE_DIR!!" | sed 's/.\{4\}$//' | fzf --preview="pass show {}" --preview-window=right:70%:wrap --query="$FZFQUERY" -e)
    else
        FZFFIND=$(find $PASSWORD_STORE_DIR -name "*.gpg" | sed "s!$PASSWORD_STORE_DIR!!" | sed 's/.\{4\}$//' | fzf -e --query="$FZFQUERY")
    fi 


    if [ -z "$FZFFIND" ]
    then
        echo "No selection detected, aborting."
    else
        if [[ $SHOW = 1 ]]
        then
            echo $FZFFIND
            echo "----------"
            pass show $FZFFIND
        else
            echo $FZFFIND
            echo "----------"
            pass -c $FZFFIND
        fi
    fi
}

[[ "$1" == "help" || "$1" == "--help" || "$1" == "-h" ]] && cmd_fzf_usage
cmd_fzf "$@"
